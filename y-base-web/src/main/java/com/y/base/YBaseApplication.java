package com.y.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan()
@SpringBootApplication
public class YBaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(YBaseApplication.class, args);
    }

}

